
#include "eeprom.h"
#include "stm32f0xx_hal_i2c.h"
//#define I2C_DEBUG_ENABLE

__IO uint16_t  sEEAddress = EXT_EEPROM_HW_ADDR;   
__IO uint32_t  sEETimeout = I2CTIMEOUT;
__IO uint16_t  sEEDataNum;
uint32_t I2CTimeOut = I2CTIMEOUT;

FlagStatus I2C_GetFlagStatus(I2C_TypeDef* I2Cx, uint32_t I2C_FLAG)
{
  uint32_t tmpreg = 0;
  FlagStatus bitstatus = RESET;
  
  /* Check the parameters */
  assert_param(IS_I2C_ALL_PERIPH(I2Cx));
  assert_param(IS_I2C_GET_FLAG(I2C_FLAG));
  
  /* Get the ISR register value */
  tmpreg = I2Cx->ISR;
  
  /* Get flag status */
  tmpreg &= I2C_FLAG;
  
  if(tmpreg != 0)
  {
    /* I2C_FLAG is set */
    bitstatus = SET;
  }
  else
  {
    /* I2C_FLAG is reset */
    bitstatus = RESET;
  }
  return bitstatus;
} 
void I2C_TransferHandling(I2C_TypeDef* I2Cx, uint16_t Address, uint8_t Number_Bytes, uint32_t ReloadEndMode, uint32_t StartStopMode)
{
  uint32_t tmpreg = 0;
  
  /* Check the parameters */
  assert_param(IS_I2C_ALL_PERIPH(I2Cx));
  assert_param(IS_I2C_SLAVE_ADDRESS(Address));  
  assert_param(IS_RELOAD_END_MODE(ReloadEndMode));
  assert_param(IS_START_STOP_MODE(StartStopMode));
    
  /* Get the CR2 register value */
  tmpreg = I2Cx->CR2;
  
  /* clear tmpreg specific bits */
  tmpreg &= (uint32_t)~((uint32_t)(I2C_CR2_SADD | I2C_CR2_NBYTES | I2C_CR2_RELOAD | I2C_CR2_AUTOEND | I2C_CR2_RD_WRN | I2C_CR2_START | I2C_CR2_STOP));
  
  /* update tmpreg */
  tmpreg |= (uint32_t)(((uint32_t)Address & I2C_CR2_SADD) | (((uint32_t)Number_Bytes << 16 ) & I2C_CR2_NBYTES) | \
            (uint32_t)ReloadEndMode | (uint32_t)StartStopMode);
  
  /* update CR2 register */
  I2Cx->CR2 = tmpreg;  
}

	void I2C_ClearFlag(I2C_TypeDef* I2Cx, uint32_t I2C_FLAG)
{ 
  /* Check the parameters */
  assert_param(IS_I2C_ALL_PERIPH(I2Cx));
  assert_param(IS_I2C_CLEAR_FLAG(I2C_FLAG));

  /* Clear the selected flag */
  I2Cx->ICR = I2C_FLAG;
  }

void I2C_SendData(I2C_TypeDef* I2Cx, uint8_t Data)
{
  /* Check the parameters */
  assert_param(IS_I2C_ALL_PERIPH(I2Cx));
  
  /* Write in the DR register the data to be sent */
  I2Cx->TXDR = (uint8_t)Data;
}

	uint8_t I2C_ReceiveData(I2C_TypeDef* I2Cx)
{
  /* Check the parameters */
  assert_param(IS_I2C_ALL_PERIPH(I2Cx));
  
  /* Return the data in the DR register */
  return (uint8_t)I2Cx->RXDR;
} 

void I2C_GenerateSTART(I2C_TypeDef* I2Cx, FunctionalState NewState)
{
  /* Check the parameters */
  assert_param(IS_I2C_ALL_PERIPH(I2Cx));
  assert_param(IS_FUNCTIONAL_STATE(NewState));
  
  if (NewState != DISABLE)
  {
    /* Generate a START condition */
    I2Cx->CR2 |= I2C_CR2_START;
  }
  else
  {
    /* Disable the START condition generation */
    I2Cx->CR2 &= (uint32_t)~((uint32_t)I2C_CR2_START);
  }
}  


//extern I2C_InitTypeDef  I2C_InitStructure;

//void I2C_Cmd(I2C_TypeDef* I2Cx, FunctionalState NewState)
//{
  /* Check the parameters */
 // assert_param(IS_I2C_ALL_PERIPH(I2Cx));
//  assert_param(IS_FUNCTIONAL_STATE(NewState));
//  if (NewState != DISABLE)
//  {
    /* Enable the selected I2C peripheral */
//    I2Cx->CR1 |= I2C_CR1_PE;
//  }
//  else
 // {
    /* Disable the selected I2C peripheral */
   // I2Cx->CR1 &= (uint32_t)~((uint32_t)I2C_CR1_PE);
//  }
//}

uint8_t i2c_error_callback(void)
{
	//I2C_ClearFlag(I2C2,I2C_ICR_STOPCF); 
	//I2C_SoftwareResetCmd(I2C1);
	/* I2C2 Peripheral Disable */
 // I2C_Cmd(I2C2, DISABLE);
 
  /* I2C2 DeInit */
//  HAL_I2C_DeInit(I2C2);

  /* I2C2 Periph clock disable */
 // RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, DISABLE);
	
  //	I2C_init();
   printf("\n\rerror_Calback");
	return 0xFE;
}
	


/**
  ******************************************************************************
  * @author  Atoll Team
  * @version V0.1.5.7
  * @date    17-Oct-2017
  * @brief   Write data to external EEPROM
  * @args	   addr:address of RTC register
  * @args	   *data:ptr to data to write
	* @args	   datalen:length of data to write
						 NB:max no of bytes that can be write is 255
  * @return	 -Nil-
  ******************************************************************************/
uint32_t sEE_WritePage(uint8_t* pBuffer, uint16_t WriteAddr, uint8_t* NumByteToWrite)
{   
  uint32_t DataNum = 0;
  //printf("\n\rsEE_WritePage1");
	#if 1
  /* Configure slave address, nbytes, reload and generate start */
  I2C_TransferHandling(I2C2, sEEAddress, 1,  I2C_CR2_RELOAD,   I2C_CR2_START);
  
  /* Wait until TXIS flag is set */
  sEETimeout = I2CTIMEOUT;  
  while(I2C_GetFlagStatus(I2C2, I2C_ISR_TXIS) == RESET)
  {
    if((sEETimeout--) == 0) return i2c_error_callback();
  }
  
  /* Send memory address */
	//printf("\n\rsEE_WriteAddr:%d",WriteAddr);
	
  I2C_SendData(I2C2, (uint8_t)WriteAddr);
	#endif
	#if 0
	
	/* Configure slave address, nbytes, reload and generate start */
  I2C_TransferHandling(I2C2, sEEAddress, 2, I2C_Reload_Mode, I2C_Generate_Start_Write);
  
  /* Wait until TXIS flag is set */
  sEETimeout = I2CTIMEOUT;
  while(I2C_GetFlagStatus(I2C2, I2C_ISR_TXIS) == RESET)
  {
    if((sEETimeout--) == 0) return i2c_error_callback();
  }
  
  /* Send MSB of memory address */
  I2C_SendData(I2C2, (uint8_t)((WriteAddr & 0xFF00) >> 8));  
  
  /* Wait until TXIS flag is set */
  sEETimeout = I2CTIMEOUT;
  while(I2C_GetFlagStatus(I2C2, I2C_ISR_TXIS) == RESET)
  {
    if((sEETimeout--) == 0) return i2c_error_callback();
  }
  
  /* Send LSB of memory address  */
  I2C_SendData(I2C2, (uint8_t)(WriteAddr & 0x00FF));
	
	#endif
  
  /* Wait until TCR flag is set */
  sEETimeout = I2CTIMEOUT;
  while(I2C_GetFlagStatus(I2C2, I2C_ISR_TCR) == RESET)
  {
    if((sEETimeout--) == 0) return i2c_error_callback();
  }
   //printf("\n\rsEE_WritePage3");
  /* Update CR2 : set Slave Address , set write request, generate Start and set end mode */
  I2C_TransferHandling(I2C2, sEEAddress, (uint8_t)(*NumByteToWrite), I2C_CR2_AUTOEND, (0x00000000U));
  
  while (DataNum != (*NumByteToWrite))
  {      
    /* Wait until TXIS flag is set */
    sEETimeout = I2CTIMEOUT;
    while(I2C_GetFlagStatus(I2C2, I2C_ISR_TXIS) == RESET)
    {
      if((sEETimeout--) == 0) return i2c_error_callback();
    }  
    
    /* Write data to TXDR */
    I2C_SendData(I2C2, (uint8_t)(pBuffer[DataNum]));
		//printf("\n\rSendData:%x",pBuffer[DataNum]);
    
    /* Update number of transmitted data */
    DataNum++;   
  }  
  
  /* Wait until STOPF flag is set */
  sEETimeout = I2CTIMEOUT;
  while(I2C_GetFlagStatus(I2C2, I2C_ISR_STOPF) == RESET)
  {
    if((sEETimeout--) == 0) return i2c_error_callback();
  }   
  
  /* Clear STOPF flag */
  I2C_ClearFlag(I2C2, I2C_ICR_STOPCF);
  
  /* If all operations OK, return sEE_OK (0) */
  return sEE_OK;
}




	uint32_t sEE_WaitEepromStandbyState(uint16_t Address)      
{
  __IO uint32_t sEETrials = 0;
  
  /* Keep looping till the slave acknowledge his address or maximum number 
  of trials is reached (this number is defined by sEE_MAX_TRIALS_NUMBER define
  in stm32373c_eval_i2c_ee.h file) */
  
  /* Configure CR2 register : set Slave Address and end mode */
 // I2C_TransferHandling(I2C2, Address, 0, I2C_AutoEnd_Mode, I2C_No_StartStop);  // nistha remove 
	    I2C_TransferHandling(I2C2, Address, 0,  I2C_CR2_AUTOEND, (0x00000000U)); 
  
  do
  { 
    /* Initialize sEETimeout */
    I2CTimeOut = FLAG_TIMEOUT;
    
    /* Clear NACKF */
    I2C_ClearFlag(I2C2, I2C_ICR_NACKCF | I2C_ICR_STOPCF);
    
    /* Generate start */
    I2C_GenerateSTART(I2C2, ENABLE);
    
    /* Wait until timeout elapsed */
    while (I2CTimeOut-- != 0); 
    
    /* Check if the maximum allowed number of trials has bee reached */
    if (sEETrials++ == sEE_MAX_TRIALS_NUMBER)
    {
      /* If the maximum number of trials has been reached, exit the function */
      return i2c_error_callback();
    }
  }
  while(I2C_GetFlagStatus(I2C2, I2C_ISR_NACKF) != RESET);
  
  /* Clear STOPF */
  I2C_ClearFlag(I2C2, I2C_ICR_STOPCF);
  
  /* Return sEE_OK if device is ready */
  return sEE_OK;
}
void WriteExtEeprom(uint16_t WriteAddr,uint16_t NumByteToWrite,uint8_t* pBuffer)
{   
	 uint32_t cnt=0;
   if(WriteAddr > 255)
		sEEAddress = 0xA2;
	else
		sEEAddress = 0xA0;
	
   sEE_WaitEepromStandbyState(sEEAddress); // Done
	 sEEDataNum = 1;
	
	 while(NumByteToWrite)
	 {
		//printf("address:%d,Data:%x\n\r",WriteAddr,*(pBuffer+cnt)); 
	  //WriteOneByteEeprom(WriteAddr,*(pBuffer+cnt));
		sEE_WritePage((pBuffer+cnt), WriteAddr, (uint8_t*)(&sEEDataNum));//Done
		NumByteToWrite--;
		WriteAddr++; 
    cnt++;
		 
		if(WriteAddr > 255)
		  sEEAddress = 0xA2;
	  else
		  sEEAddress = 0xA0;
		
		sEE_WaitEepromStandbyState(sEEAddress); //Done
   
	 }
}

/**
  ******************************************************************************
  * @author  Atoll Team
  * @version V0.1.5.7
  * @date    17-Oct-2017
  * @brief   Read data from external EEPROM
  * @args	   addr:address of RTC register
  * @args	   *data:ptr to data to read
	* @args	   datalen:length of data to read 
						 NB:max no of bytes that can be read is 255
  * @return	 -Nil-
  ******************************************************************************/
void ReadExtEeprom(uint16_t addr,uint16_t datalen,uint8_t *data)
{
	uint32_t cnt=0;
	if(addr > 255)
		sEEAddress = 0xA2;
	else
		sEEAddress = 0xA0;
	
	sEE_WaitEepromStandbyState(sEEAddress);//Done
	 while(datalen)
	 {
		 //sEE_WaitEepromStandbyState(sEEAddress);
	  *(data+cnt) = ReadOneByteEeprom(addr); //Done
		 //printf("address:%d,Data:%x\n\r",addr,*(data+cnt));
		 addr++;
		datalen--; 
		cnt++; 
		 if(addr > 255)
		  sEEAddress = 0xA2;
	  else
		  sEEAddress = 0xA0;
	 }
}

/**
  ******************************************************************************
  * @author  Atoll Team
  * @version V0.1
  * @date    22-June-2018
  * @brief   Single byte write from External EEPROM
  * @args	   addr:Address of EEPROM
  * @args	   data:data of EEPROM
  * @return	 -Nil-
  ******************************************************************************/
/*void WriteOneByteEeprom(uint16_t addr, uint8_t data)
{   
    I2C_TransferConfig(hi2c, DevAddress, hi2c->XferSize, xfermode, I2C_GENERATE_START_WRITE);
	
	   I2CTimeOut = I2CTIMEOUT;
	  //Ensure the transmit interrupted flag is set
    while(I2C_GetFlagStatus(I2C2, I2C_FLAG_TXIS) == RESET)
		{
			#ifdef DEBUG
					printf("\r\nI2C_FLAG_TXIS0\r\n");
		  #endif
			if((I2CTimeOut--)==0)printf("\r\ntimout");break;
		}
		
    I2C_SendData(I2C2,(uint8_t)addr);
		
		I2CTimeOut = I2CTIMEOUT;
		
		while(I2C_GetFlagStatus(I2C2, I2C_ISR_TCR) == RESET)
		{
			#ifdef DEBUG
					printf("\r\nI2C_ISR_TCR\r\n");
		  #endif
			if((I2CTimeOut--)==0)printf("\r\ntimout");break;
		}
		
		I2C_TransferHandling(I2C2, sEEAddress, 1, I2C_AutoEnd_Mode, I2C_No_StartStop);
		
		I2CTimeOut = I2CTIMEOUT;
		
		while(I2C_GetFlagStatus(I2C2, I2C_ISR_TXIS) == RESET)
		{
			#ifdef DEBUG
					printf("\r\nI2C_ISR_TXIS\r\n");
		  #endif
			if((I2CTimeOut--)==0)printf("\r\ntimout");break;
		}
    //Send the value you wish you write to the register
    I2C_SendData(I2C2,(uint8_t)data);
		
		I2CTimeOut = I2CTIMEOUT;
		
    while(I2C_GetFlagStatus(I2C2, I2C_ISR_STOPF) == RESET)
		{
			#ifdef DEBUG
					printf("\r\nI2C_ISR_STOPF\r\n");
		  #endif
			if((I2CTimeOut--)==0)printf("\r\ntimout");break;
		}
    //Clear the stop flag for the next potential transfer
    I2C_ClearFlag(I2C2, I2C_ICR_STOPCF);
		
		#ifdef DEBUG
					//printf("\r\nI2C_ClearFlag\r\n");
		#endif
		//sEE_WaitEepromStandbyState(sEEAddress);*/
		
//		if(!I2CTimeOut)
//		{
//			#ifdef DEBUG
//					//printf("\r\ni2c_error_callback\r\n");
//		  #endif
//			i2c_error_callback();
//		}
		
//}
uint8_t ReadOneByteEeprom(uint16_t Regaddr)
{
    uint8_t tempdata;
	  #ifdef DEBUG
					//printf("\r\nRTC_Gettime\r\n");
		#endif
    //sEE_WaitEepromStandbyState(sEEAddress);
	
    I2C_TransferHandling(I2C2, sEEAddress, 1,  (0x00000000U), I2C_CR2_START); //Done
	  I2CTimeOut = I2CTIMEOUT;
    while(I2C_GetFlagStatus(I2C2, I2C_ISR_TXIS) == RESET)
		{
			#ifdef DEBUG
//					printf("\r\nI2C_ISR_TXIS\r\n");
		  #endif
	   if((I2CTimeOut--)==0)break;
		}
    I2C_SendData(I2C2,(uint8_t)Regaddr);
		I2CTimeOut = I2CTIMEOUT;
    while(I2C_GetFlagStatus(I2C2, I2C_ISR_TC)==RESET)
		{
			#ifdef DEBUG
//					printf("\r\nI2C_ISR_TC\r\n");
		  #endif
	   if((I2CTimeOut--)==0)break;
		}
	
      I2C_TransferHandling(I2C2, sEEAddress, 1,  I2C_CR2_AUTOEND, (uint32_t)(I2C_CR2_START | I2C_CR2_RD_WRN));
		I2CTimeOut = I2CTIMEOUT;
    while(I2C_GetFlagStatus(I2C2, I2C_ISR_RXNE) == RESET)
		{
			#ifdef DEBUG
//					printf("\r\nI2C_ISR_RXNE\r\n");
		  #endif
	   if((I2CTimeOut--)==0)break;
		}
//		tempdata=I2C_ReceiveData(I2C2);
//		while(I2C_GetFlagStatus(I2C2, I2C_ISR_RXNE) == RESET)
//		{
//			#ifdef DEBUG
////					printf("\r\nI2C_ISR_RXNE\r\n");
//		  #endif
//	   if((I2CTimeOut--)==0)break;
//		}
		
    tempdata=I2C_ReceiveData(I2C2);
		//printf("\n\rrecvData:%x",tempdata);
		I2CTimeOut = I2CTIMEOUT;
		while(I2C_GetFlagStatus(I2C2, I2C_ICR_STOPCF) == RESET)
		{
			#ifdef DEBUG
//					printf("\r\nI2C_ISR_RXNE\r\n");
		  #endif
	   if((I2CTimeOut--)==0)break;
		}
		
		
    I2C_ClearFlag(I2C2,I2C_ICR_STOPCF);
   
		if(I2CTimeOut)
	  {
		 #ifdef DEBUG
					//printf("\r\nI2CRead_ClearFlag\r\n");
		  #endif	
     return tempdata;
	  }
		else
		{
			i2c_error_callback();
			#ifdef DEBUG
					//printf("\r\nI2CReadTimeout_ClearFlag\r\n");
		  #endif
			return 0;
		}
}


/**
  ******************************************************************************
  * @author  Atoll Team
  * @version V0.1
  * @date    22-June-2018
  * @brief   Single byte read from External EEPROM
  * @args	   Regaddr:Address of EEPROM
  * @return	 -Nil-
  ******************************************************************************/
