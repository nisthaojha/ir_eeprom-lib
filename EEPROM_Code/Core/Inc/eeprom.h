


#ifndef EXT_EEPROM
#define EXT_EEPROM

#include "main.h"


#define EXT_EEPROM_HW_ADDR            0xA0
#define FLAG_TIMEOUT         ((uint32_t)0x1000)
#define I2CTIMEOUT         ((uint32_t)(10 * FLAG_TIMEOUT))
#define sEE_PAGESIZE           16
      
#define sEE_OK                    0
#define sEE_FAIL                  1 

extern uint32_t I2CTimeOut;

#define sEE_MAX_TRIALS_NUMBER     300
      
#define sEE_OK                    0
#define sEE_FAIL                  1

//#define SIGNATURE_ADDRS								  0		//Each address holds 2 byte of data(Total signature data size 2 bytes for storing Firmware version)
//#define UUIID_ADDRS										  2   //UUID 16 bytes
//#define MACID_STAT											18 //MACID status 1 byte(For checking MACID already stored or not)
//#define WM_MODE_ADDRS										19  //Working Mode Address	size 1 byte
////#define ROUTER_PARAM_ADDRESS					  20	 //Router param size 98 bytes
////#define SERVER_PARAM_ADDRESS					  118  //Server Param size 64 bytes
////#define AP_PARAM_ADDRESS					      182		//AP param size 98 bytes
////#define WIFI_SLEEPFLAG_ADDRESS			    280 //Wifi sleep Address 1 byte
//#define SPEED_LEVEL_ADDRESS							281
//#define ON_OFF_ADRESS										282
//#define TIMER_DATE_TIME_STAMP           283

//uint32_t WriteExtEeprom(uint8_t WriteAddr,uint8_t NumByteToWrite,uint8_t* pBuffer);
//uint32_t ReadExtEeprom(uint8_t ReadAddr,uint8_t NumByteToRead,uint8_t* pBuffer);
uint8_t i2c_error_callback(void);
void I2C_SendData(I2C_TypeDef* I2Cx, uint8_t Data);
#if 1
void WriteOneByteEeprom(uint16_t addr, uint8_t data);
uint8_t ReadOneByteEeprom(uint16_t Regaddr);
 void ReadExtEeprom(uint16_t addr,uint16_t datalen,uint8_t *data);
//uint32_t ReadExtEeprom(uint16_t ReadAddr, uint16_t NumByteToRead,uint8_t* pBuffer);
//void WriteExtEeprom(uint16_t addr,uint8_t datalen,uint8_t *data);
void WriteExtEeprom(uint16_t WriteAddr,uint16_t NumByteToWrite,uint8_t* pBuffer);
uint32_t sEE_WaitEepromStandbyState(uint16_t Address);
uint32_t sEE_WritePage(uint8_t* pBuffer, uint16_t WriteAddr, uint8_t* NumByteToWrite);
void I2C_ClearFlag(I2C_TypeDef* I2Cx, uint32_t I2C_FLAG);
void I2C_GenerateSTART(I2C_TypeDef* I2Cx, FunctionalState NewState);
FlagStatus I2C_GetFlagStatus(I2C_TypeDef* I2Cx, uint32_t I2C_FLAG);
uint8_t I2C_ReceiveData(I2C_TypeDef* I2Cx);
#endif
#endif
